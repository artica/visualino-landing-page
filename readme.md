
you need node v0.12.7

download from `http://nodejs.org`

then install grunt `npm install grunt-cli -g`

then install bower `npm install bower -g`

then install the project dependencies `npm install`

`grunt serve:dist` builds and serves the project on port 3000
