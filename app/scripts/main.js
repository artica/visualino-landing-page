// jshint devel:true
(function($, window) {

  var Analytics = {
    init: function() {
      (function(i,s,o,g,r,a,m){
        i['GoogleAnalyticsObject']=r;
        i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();
          a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];
          a.async=1;
          a.src=g;
          m.parentNode.insertBefore(a,m);
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-45360427-1', 'artica.cc');
      ga('send', 'pageview');

      console.log('%cAnalytics module OK!', 'color: green;');
    }
  };

  var Mailchimp = {
    init: function() {
      (function($) {
        window.fnames = new Array();
        window.ftypes = new Array();
        fnames[0]='EMAIL';
        ftypes[0]='email';
        fnames[1]='FNAME';
        ftypes[1]='text';
        fnames[2]='LNAME';
        ftypes[2]='text';
      }(jQuery));
      var $mcj = jQuery.noConflict(true);
      console.log('%cMailchimp module OK!', 'color: green;');
    }
  };

  var Scrolling = {
    init: function() {
      $(function() {
        $('.nav a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });
      });
      console.log('%cScrolling module OK!', 'color: green;');
    }
  };

  var Map = {
    canvas: $('#google-maps'),
    latitude: 38.660733,
    longitude: -9.203067,
    centerLatitude: 38.681624,
    centerLongitude: -9.198475,
    zoom: 13,
    center: function() {
      return {
        'center': Map.centerLatitude + ',' + this.centerLongitude,
        'zoom': this.zoom,
        'scrollwheel': false,
        'mapTypeId': google.maps.MapTypeId.SATELLITE
      };
    },
    marker: function() {
      return {
        'position': this.latitude + ',' + this.longitude,
        'bounds': false,
        'draggable': false
      };
    },
    init: function() {
      if (typeof google !== 'undefined') {
        Map.canvas
          .gmap(Map.center())
          .bind('init', function() {
            Map.canvas.gmap('addMarker', Map.marker())
          });
      }
      console.log('%cMap module OK!', 'color: green;');
    }
  };

  var Visualino = {
    init: function() {
      Analytics.init();
      Mailchimp.init();
      Scrolling.init();
      Map.init();
      console.log('%cVisualino OK!', 'color: green;');
    }
  };

  $(document).ready(Visualino.init);
  
})(jQuery, window, undefined);